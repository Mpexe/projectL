using UnrealBuildTool;

public class ProjectLTarget : TargetRules
{
	public ProjectLTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("ProjectL");
	}
}
